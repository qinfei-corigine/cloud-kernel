/* SPDX-License-Identifier: GPL-2.0 */
/*
 *  Authors: Wang Yinfeng <wangyinfenng@phytium.com.cn>
 *
 *  Copyright (C) 2021, PHYTIUM Information Technology Co., Ltd.
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 */

#ifndef _MACHINE_TYPE_H_
#define _MACHINE_TYPE_H_

#define typeof_ft1500a()	0
#define typeof_ft2000ahk()	0
#define typeof_ft2000plus()	0
#define typeof_ft2004()		0
#define typeof_s2500()		0

#endif
