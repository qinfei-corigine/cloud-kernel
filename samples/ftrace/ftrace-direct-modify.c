// SPDX-License-Identifier: GPL-2.0-only
#include <linux/module.h>
#include <linux/kthread.h>
#include <linux/ftrace.h>

extern void my_direct_func1(void);
extern void my_direct_func2(void);

void my_direct_func1(void)
{
	trace_printk("my direct func1\n");
}

void my_direct_func2(void)
{
	trace_printk("my direct func2\n");
}

extern void my_tramp1(void *);
extern void my_tramp2(void *);

static unsigned long my_ip = (unsigned long)schedule;

#ifdef CONFIG_X86_64

asm (
"	.pushsection    .text, \"ax\", @progbits\n"
"	.type		my_tramp1, @function\n"
"	.globl		my_tramp1\n"
"   my_tramp1:"
"	pushq %rbp\n"
"	movq %rsp, %rbp\n"
"	call my_direct_func1\n"
"	leave\n"
"	.size		my_tramp1, .-my_tramp1\n"
	ASM_RET
"	.type		my_tramp2, @function\n"
"	.globl		my_tramp2\n"
"   my_tramp2:"
"	pushq %rbp\n"
"	movq %rsp, %rbp\n"
"	call my_direct_func2\n"
"	leave\n"
	ASM_RET
"	.size		my_tramp2, .-my_tramp2\n"
"	.popsection\n"
);

#endif /* CONFIG_X86_64 */

#ifdef CONFIG_ARM64

asm (
"	.pushsection    .text, \"ax\", @progbits\n"
"	.type		my_tramp1, @function\n"
"	.globl		my_tramp1\n"
"   my_tramp1:"
"	hint	34\n" // bti	c
"	sub	sp, sp, #16\n"
"	stp	x9, x30, [sp]\n"
"	bl	my_direct_func1\n"
"	ldp	x30, x9, [sp]\n"
"	add	sp, sp, #16\n"
"	ret	x9\n"
"	.size		my_tramp1, .-my_tramp1\n"

"	.type		my_tramp2, @function\n"
"	.globl		my_tramp2\n"
"   my_tramp2:"
"	hint	34\n" // bti	c
"	sub	sp, sp, #16\n"
"	stp	x9, x30, [sp]\n"
"	bl	my_direct_func2\n"
"	ldp	x30, x9, [sp]\n"
"	add	sp, sp, #16\n"
"	ret	x9\n"
"	.size		my_tramp2, .-my_tramp2\n"
"	.popsection\n"
);

#endif /* CONFIG_ARM64 */

static struct ftrace_ops direct;

static unsigned long my_tramp = (unsigned long)my_tramp1;
static unsigned long tramps[2] = {
	(unsigned long)my_tramp1,
	(unsigned long)my_tramp2,
};

static int simple_thread(void *arg)
{
	static int t;
	int ret = 0;

	while (!kthread_should_stop()) {
		set_current_state(TASK_INTERRUPTIBLE);
		schedule_timeout(2 * HZ);

		if (ret)
			continue;
		t ^= 1;
		ret = modify_ftrace_direct(&direct, tramps[t]);
		if (!ret)
			my_tramp = tramps[t];
		WARN_ON_ONCE(ret);
	}

	return 0;
}

static struct task_struct *simple_tsk;

static int __init ftrace_direct_init(void)
{
	int ret;

	ftrace_set_filter_ip(&direct, (unsigned long) my_ip, 0, 0);
	ret = register_ftrace_direct(&direct, my_tramp);

	if (!ret)
		simple_tsk = kthread_run(simple_thread, NULL, "event-sample-fn");
	return ret;
}

static void __exit ftrace_direct_exit(void)
{
	kthread_stop(simple_tsk);
	unregister_ftrace_direct(&direct, my_tramp, true);
}

module_init(ftrace_direct_init);
module_exit(ftrace_direct_exit);

MODULE_AUTHOR("Steven Rostedt");
MODULE_DESCRIPTION("Example use case of using modify_ftrace_direct()");
MODULE_LICENSE("GPL");
